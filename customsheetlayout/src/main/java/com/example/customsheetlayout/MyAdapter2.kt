package com.example.customsheetlayout

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_column.view.*
import kotlinx.android.synthetic.main.item_row.view.*

class MyAdapter2: RecyclerView.Adapter<MyAdapter2.ViewHolder>() {
    var dataList: ArrayList<ArrayList<String>> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.column1.setText(dataList[position][0])
        holder.column2.setText(dataList[position][1])
        holder.column3.setText(dataList[position][2])
        holder.column4.setText(dataList[position][3])
    }

    override fun getItemCount(): Int = dataList.size


    class ViewHolder(rootView: View): RecyclerView.ViewHolder(rootView){
        val column1 = rootView.custom_column_1
        val column2 = rootView.custom_column_2
        val column3 = rootView.custom_column_3
        val column4 = rootView.custom_column_4

    }
}