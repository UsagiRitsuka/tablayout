package com.example.customsheetlayout

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_column.view.*

class MyAdapter1: RecyclerView.Adapter<MyAdapter1.ViewHolder>(){
    var dataList: ArrayList<String> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_column, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.text.text = dataList[position]
    }

    override fun getItemCount(): Int = dataList.size


    class ViewHolder(rootView: View): RecyclerView.ViewHolder(rootView){
        val text = rootView.column_name

    }
}