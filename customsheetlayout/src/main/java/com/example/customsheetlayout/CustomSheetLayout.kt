package com.example.customsheetlayout

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.sheet.view.*

class CustomSheetLayout: FrameLayout {
    val datalist1 = arrayListOf<String>("aaa111", "aaa222", "aaa333", "aaa444", "aaa555", "aaa333", "aaa444", "aaa555")
    val datalist2 = arrayListOf<ArrayList<String>>(
        arrayListOf<String>("bbb111", "bbb222", "bbb333", "bbb444", "bbb555"),
        arrayListOf<String>("ccc111", "ccc222", "ccc333", "ccc444", "ccc555"),
        arrayListOf<String>("bbb111", "bbb222", "bbb333", "bbb444", "bbb555"),
        arrayListOf<String>("ccc111", "ccc222", "ccc333", "ccc444", "ccc555"),
        arrayListOf<String>("bbb111", "bbb222", "bbb333", "bbb444", "bbb555"),
        arrayListOf<String>("ccc111", "ccc222", "ccc333", "ccc444", "ccc555"),
        arrayListOf<String>("bbb111", "bbb222", "bbb333", "bbb444", "bbb555"),
        arrayListOf<String>("ccc111", "ccc222", "ccc333", "ccc444", "ccc555")
    )

    private val adapter1 = MyAdapter1()
    private val adapter2 = MyAdapter2()

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.sheet, this, true)
        setupViewCompinent()
    }

    private fun setupViewCompinent(){
        first_recycler_view.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = adapter1
        }

        main_recycler_view.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = adapter2
        }

        main_scroller.setOnScrollChangeListener { view, l, t, r, b ->

        }

    }

    fun setData(){
        adapter1.dataList = datalist1
        adapter2.dataList = datalist2
    }

    fun notifyDataSetChanged(){
        adapter1.notifyDataSetChanged()
        adapter2.notifyDataSetChanged()
    }

    override fun onLayout(p0: Boolean, l: Int, t: Int, r: Int, b: Int) {
        for (i in 0 until childCount) {
            getChildAt(i).layout(l, t, r, b)
        }
    }

}