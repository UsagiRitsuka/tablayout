package com.example.teblelayoutproj

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.constraintlayout.widget.ConstraintLayout
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



class CustomLayout: FrameLayout {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0){
        Log.v("CustomLayout", "constructor2")
        LayoutInflater.from(context).inflate(R.layout.custom, this, true)
    }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        Log.v("CustomLayout", "constructor3")
        LayoutInflater.from(context).inflate(R.layout.custom, this, true)

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        Log.v("CustomLayout", "onMeasure")
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        Log.v("CustomLayout", "ondraw")
    }

    override fun onLayout(p0: Boolean, l: Int, t: Int, r: Int, b: Int) {
        Log.v("CustomLayout", "onLayout")
        for (i in 0 until childCount) {
            getChildAt(i).layout(l, t, r, b)
        }
    }

}